#Reverse engineering Tektronix TDS 3054

Inspired from EEVblog Dave Jones videos:  
EEVblog #564 <http://www.youtube.com/watch?v=leC-zSMbtfg>  
EEVblog #565 <http://www.youtube.com/watch?v=qXGqDDE9-4M>

Files are made with libreoffice--draw and libreoffice--calc

EEVblog forum threads:  
<http://www.eevblog.com/forum/blog/eevblog-564-tektronix-tds3054-oscilloscope-teardown-repair/>  
<http://www.eevblog.com/forum/blog/eevblog-565-tektronix-tds3054-oscilloscope-repair-part-2/>
